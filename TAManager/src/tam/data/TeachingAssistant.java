package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty name;
    private final StringProperty email;
    /**
     * Constructor initializes the TA name
     */
    public TeachingAssistant(String initName, String initEmail) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    
    public StringProperty getNameProp(){
        return name;
    }
    public StringProperty getEmailProp(){
        return email;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String initName) {
        name.set(initName);
    }
    //GETTER AND SETTER -> EMAIL
    
    public String getEmail(){
        return email.get();
    }
    public void setEmail(String initEmail){
        email.set(initEmail);
    }
    
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
    }
    /*
    Alter toString() method to display both name and email
    - will do later
    */
    @Override
    public String toString() {
        return name.getValue();
    }
    //Make equals method to compare two TA objects
    //@Override
    public boolean equals(TeachingAssistant ta){
        if(this.getEmail().equals(ta.getEmail()) && this.getName().equals(ta.getName())){
            return true;
        } else{
            return false;
        }
    }
}